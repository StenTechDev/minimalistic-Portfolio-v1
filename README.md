# Minimalistic Portfolio Website version 1
This is my first portfolio website created with the guidance from Dr. Angela Yu of The App Brewery. I have altered the code to suit my needs and style.

This is a minimalistic, html portfolio website.

To view the website, visit: http://stentechdev.com/minimalistic-portfolio.v1/
